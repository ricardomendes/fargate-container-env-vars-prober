# AWS Fargate container environment variables prober

A very simple OCI-compliant image designed to record the environment variables
provided by the container runtime into [Amazon CloudWatch Logs](https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/WhatIsCloudWatchLogs.html).

[![pipeline
status](https://gitlab.com/ricardomendes/fargate-container-env-vars-prober/badges/master/pipeline.svg)](https://gitlab.com/ricardomendes/fargate-container-env-vars-prober/-/commits/master)

Extra features:

- [Fargate Task's temporary security credentials](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task-iam-roles.html)
  are fetched and stored into the `~/.aws/credentials` file when a container is
  started;
- containers from this image are accessible through SSH.

## Task Definition setup

1. Enable [Amazon CloudWatch Logs](https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/WhatIsCloudWatchLogs.html)

   ```json
   {
     ...,
     "containerDefinitions": [
       {
         ...,
         "logConfiguration": {
           "logDriver": "awslogs",
           "options": {
             "awslogs-group": "/ecs/fargate-env-vars-prober",
             "awslogs-region": "<REGION-ID>",
             "awslogs-stream-prefix": "ecs"
           }
         },
         ...
       }
     ],
     ...
   }
   ```

1. Enable default SSH port forwarding

   ```json
   {
     ...,
     "containerDefinitions": [
       {
         ...,
         "portMappings": [
           {
             "hostPort": 22,
             "protocol": "tcp",
             "containerPort": 22
           }
         ],
         ...
       }
     ],
     ...
   }
   ```

1. Inject the `SSH_PUBLIC_KEY` environment variable, reading its value from [AWS
   Systems Manager Parameter Store](https://docs.aws.amazon.com/systems-manager/latest/userguide/systems-manager-parameter-store.html)

   ```json
   {
     ...,
     "containerDefinitions": [
       {
         ...,
         "secrets": [
           {
             "valueFrom": "arn:aws:ssm:<REGION-ID>:<ACCOUNT-ID>:parameter/<PARAMETER-ID>",
             "name": "SSH_PUBLIC_KEY"
           }
         ],
         ...
       }
     ],
     ...
   }
   ```
