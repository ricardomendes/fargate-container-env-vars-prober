FROM ubuntu:20.04

# ---------------------------------------------------------------------------- #
# Install and configure tools to fetch and parse Amazon ECS temporary/unique   #
# security credentials, which come in the JSON format. 'jq' is a tool to       #
# handle JSON objects in shell scripts.                                        #
# ---------------------------------------------------------------------------- #
RUN apt-get update -qq -y \
    && apt-get install -qq -y curl jq

# ---------------------------------------------------------------------------- #
# Install and configure sshd.                                                  #
# https://docs.docker.com/engine/examples/running_ssh_service for reference.   #
# ---------------------------------------------------------------------------- #
RUN apt-get install -qq -y openssh-server \
    && mkdir -p /var/run/sshd

EXPOSE 22

# ---------------------------------------------------------------------------- #
# Execute a startup script.                                                    #
# https://success.docker.com/article/use-a-script-to-initialize-stateful-container-data
# for reference.                                                               #
# ---------------------------------------------------------------------------- #
COPY docker-entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-entrypoint.sh
ENTRYPOINT ["docker-entrypoint.sh"]
